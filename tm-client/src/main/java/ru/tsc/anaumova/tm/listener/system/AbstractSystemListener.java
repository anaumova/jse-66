package ru.tsc.anaumova.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.service.IPropertyService;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}