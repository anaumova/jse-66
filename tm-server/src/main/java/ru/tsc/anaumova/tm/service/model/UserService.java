package ru.tsc.anaumova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.api.service.IPropertyService;
import ru.tsc.anaumova.tm.api.service.model.IUserService;
import ru.tsc.anaumova.tm.dto.model.UserDto;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.exception.entity.UserNotFoundException;
import ru.tsc.anaumova.tm.exception.field.*;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.repository.dto.UserDtoRepository;
import ru.tsc.anaumova.tm.repository.model.UserRepository;
import ru.tsc.anaumova.tm.util.HashUtil;

import java.util.Optional;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserRepository repository;

    @NotNull
    @Override
    protected UserRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserRepository repository = getRepository();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final UserRepository repository = getRepository();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserRepository repository = getRepository();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        repository.save(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final UserRepository repository = getRepository();
        return repository.findFirstByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserRepository repository = getRepository();
        return repository.findFirstByEmail(email);
    }

    @Override
    @Transactional
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findOneById(id);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User updateUser(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserRepository repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserRepository repository = getRepository();
        repository.deleteById(id);
    }

}